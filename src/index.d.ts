import EventEmitter from 'events';
import stream from 'stream';

interface StreamOptions {
  /**
   * The length of the match/readable buffer.
   * Smaller values use less memory but may result in 
   * a slower parse time.
   * @default 1024
   */
  bufferLength?: number;
  /**
   * Set this if you would like to pass a value for the
   * `highWaterMark` option to the readable stream constructor.
   */
  highWaterMark?: number;
}

export interface ParsingOptions extends StreamOptions {
  /**
   * Transformation function, invoked depth-first
   * against the parsed data structure.
   * This option is analogous to the reviver
   * parameter for JSON.parse.
   */
  reviver?: (key: string, obj: any) => any;
  /**
   * The number of data items to process per timeslice.
   * @default 16384
   */
  yieldRate?: number;
  /**
   * The promise constructor to use, defaults to bluebird.
   */
  Promise?: typeof Promise | Function;
  /**
   * For `bfj.walk`, `bfj.match` or `bfj.parse` only.
   * Set this to true to parse newline-delimited JSON. In
   * this case, each call will be resolved with one value
   * from the stream. To parse the entire stream, calls
   * should be made sequentially one-at-a-time until the
   * returned promise resolves to `undefined`.
   * @default false;
   */
  ndjson?: boolean;
  /**
   * The number of characters to process before discarding them
   * to save memory.
   * @default 1048576
   * @deprecated because not found a single reference to this argument..
   */
  discard?: number;
};

export interface ParsingOptionsMatch extends ParsingOptions {
  /**
   * For `bfj.match` only, 
   * set this to true if you wish to match against numbers
   * with a string or regular expression selector argument
   * @default false
   */
  numbers?: boolean;
}

export interface SerializationOptions extends StreamOptions {
  /**
   * Indentation string or the number of spaces to indent
   * each nested level by. This option is analogous to the
   * space parameter for JSON.stringify.
   * @default 0
   */
  space?: string | number;
  /**
   * By default, promises are coerced to their resolved value.
   * Set this property to `ignore` for improved performance
   * if you don't need to coerce promises.
   * @default 'resolve'
   */
  promises?: 'resolve' | 'ignore';
  /** By default, maps are coerced to plain objects.
   * Set this property to `ignore` for improved performance
   * if you don't need to coerce maps.
   * @default 'object'
   */
  maps?: 'object' | 'ignore';
  /**
   * By default, other iterables (i.e. not arrays, strings or maps)
   * are coerced to arrays. 
   * Set this property to `ignore` for improved performance
   * if you don't need to coerce iterables.
   * @default 'array'
   */
  iterables?: 'array' | 'ignore';
  /**
   * By default, circular references will cause the write
   * to fail.
   * Set this property to `ignore` if you'd prefer
   * to silently skip past circular references in the data.
   * @default 'error'
   */
  circular?: 'error' | 'ignore';
  /**
   * The number of data items to process per timeslice.
   * @default 16384
   */
  yieldRate?: number;
  /**
   * The promise constructor to use, defaults to bluebird.
   */
  Promise?: typeof Promise | Function;
}

class Events {
  static array: 'arr';
  static object: 'obj';
  static property: 'pro';
  static string: 'str';
  static number: 'num';
  static literal: 'lit';
  static endPrefix: 'end-';
  static end: 'end';
  static error: 'err';
  static endArray: 'end-arr'; // endPrefix + array
  static endObject: 'end-obj'; // endPrefix + object
  static endLine: 'end-line'; // endPrefix + 'line'
  static dataError: `err-data`; // error + '-data'
}

interface JSONEventEmitterEvents {
  /**
   * `bfj.events.array` indicates that an array context
   * has been entered (f.e. by encountering the `[` character)
   */
  [Events.array]: () => void;
  /**
   * `bfj.events.endArray` indicates that an array context
   * has been entered (f.e. by encountering the `]` character)
   */
  [Events.endArray]: () => void;
  /**
   * `bfj.events.object` indicates that an array context
   * has been entered (f.e. by encountering the `{` character)
   */
  [Events.object]: () => void;
  /**
   * `bfj.events.endObject` indicates that an array context
   * has been entered (f.e. by encountering the `}` character)
   */
  [Events.endObject]: () => void;
  /**
   * `bfj.events.property` indicates that a property
   * has been encountering in an object.
   * The listener will be passed the name of the property
   * as its argument and the next event to be emitted
   * will represent the property's value.
   */
  [Events.property]: (name: string) => void;
  /**
   * `bfj.events.string` indicates that a string has been encountered.
   * The listener will be passed the value as its argument.
   */
  [Events.string]: (value: string) => void;
  /**
   * `bfj.events.number` indicates that a number has been encountered.
   * The listener will be passed the value as its argument.
   */
  [Events.number]: (value: number) => void;
  /**
   * `bfj.events.literal` indicates that a JSON literal
   * (either `true`, `false` or `null`) has been encountered.
   * The listener will be passed the value as its argument.
   */
  [Events.literal]: (value: null | boolean) => void;
  /**
   * `bfj.events.error` indicates that an error was caught
   * from one of the event handlers in user code.
   * The listener will be passed the `Error` instance as its argument.
   */
  [Events.error]: (error: Error) => void;
  /**
   * `bfj.events.dataError` indicates that a syntax error was encountered
   * in the incoming JSON stream OR circular reference was encountered
   * in the data and the `circular` option was not set to `ignore`.
   * 
   * The listener will be passed an Error instance decorated with
   * `actual`, `expected`, `lineNumber` and `columnNumber` properties
   * as its argument only in parsing functions.
   */
  [Events.dataError]: (error: Error & { actual?: any, expected?: any, lineNumber?: number, columnNumber?: number }) => void;
  /**
   * `bfj.events.end` indicates that the end of input has been reached
   * and the stream is closed.
   */
  [Events.end]: () => void;
  /**
   * `bfj.events.endLine` indicates that a root-level newline character
   * has been encountered in an NDJSON stream.
   * Only emitted if the `ndjson` option is set.
   */
  [Events.endLine]: () => void;
}

// TODO: fix comments disappearing issue in JSONEventEmitter
class JSONEventEmitter extends EventEmitter {
  on<U extends keyof JSONEventEmitterEvents>(
    event: U,
    listener: JSONEventEmitterEvents[U]
  ): this;
  emit<U extends keyof JSONEventEmitterEvents>(
    event: U,
    ...args: Parameters<JSONEventEmitterEvents[U]>
  ): boolean;
  /**
   * Both `bfj.walk` and `bfj.eventify` decorate their
   * returned event emitters with a pause method
   * that will prevent any further events being
   * emitted. The `pause` method itself returns
   * a `resume` function that you can call to indicate
   * that processing should continue.
   */
  pause(): /** resume function */ () => void;
}

/**
 * Asynchronously walks a stream of JSON data, emitting events as it encounters tokens.
 * 
 * @param stream
 */
export function walk(stream: stream.Readable, options?: ParsingOptions): JSONEventEmitter;
// TODO
export function match(stream: stream.Readable, options?: ParsingOptions): EventEmitter;
/**
 * Returns a bluebird promise and asynchronously parses a stream of JSON data.
 * If there are no syntax errors, the returned promise is resolved with
 * the parsed data.
 * If syntax errors occur, the promise is rejected with the first error.
 * 
 * @param stream a readable stream from which the JSON will be parsed
 */
export function parse(stream: stream.Readable, options?: ParsingOptions): Promise<any>;
/**
 * Returns a writeable stream that can be passed to `stream.pipe`,
 * then parses JSON data read from the stream.
 * If there are no errors, the callback is invoked with the result
 * as the second argument.
 * If errors occur, the first error is passed to * the callback
 * as the first argument.
 * 
 * @param callback Function that will be called after parsing is complete.
 */
export function unpipe(callback: (error: Error | null, data: any) => void, options: ParsingOptions): stream.Writable;
/**
 * Returns a promise and asynchronously parses a JSON file read from disk.
 * If there are no errors, the promise is resolved with the parsed data.
 * If errors occur, the promise is rejected with the first error.
 * 
 * @param path Path to the JSON file.
 */
export function read(path: string, options?: ParsingOptions): Promise<any>;
// TODO
export function eventify(data: any, options?: SerializationOptions): JSONEventEmitter;

/**
 * `streamify` returns a readable stream and asynchronously serializes a
 * data structure to JSON, pushing the result to the returned stream.
 * 
 * If there a circular reference is encountered in the data and `options.circular`
 * is not set to `ignore`, a `dataError` event will be emitted.
 * If any other errors occur, an `error` event will be emitted.
 * @param data the data structure to serialize
 * @param options 
 */
export function streamify(data: any, options?: SerializationOptions): stream.Readable & { on(event: 'dataError', handler: (error: Error) => void ): this };
/**
 * `stringify` returns a bluebird promise and asynchronously serializes
 * a data structure to a JSON string. The promise is resolved to the
 * JSON string when serialization is complete.
 * @param data the data structure to serialize
 */
export function stringify(data: any, options?: SerializationOptions): Promise<string>;
/**
 * Returns a bluebird promise and asynchronously serializes a data structure
 * to a JSON file on disk. The promise is resolved when the file has been written,
 * or rejected with the error if writing failed.
 * 
 * @param path Path to the JSON file.
 * @param data the data structure to serialize
 */
export function write(path: string, data: any, options?: SerializationOptions): Promise<void>;

export default {
  walk,
  match,
  parse,
  unpipe,
  read,
  eventify,
  streamify,
  stringify,
  write,
  events
}
